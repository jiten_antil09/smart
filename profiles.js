const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs');
const mysql = require('mysql');
var util = require('util')
const xml2js = require('xml2js');
var parser = new xml2js.Parser();
// using global mysql connection
var email = require('emailjs');
var Cryptr = require('cryptr'),
    cryptr = new Cryptr('myTotalySecretKey');
const yo = require('../config1/dfunctions');
var MySqlHost, MySqlUser, MySqlPassword, OID, POID;
var navbar1 = fs.readFileSync('jsonFile/navbarJson.json', 'utf8');
navbar1 = JSON.parse(navbar1);
const navbar = navbar1[0];
var sendermailID, Membership_EmailID;
var temp;
try {
    temp = fs.readFileSync('configuration.xml', "utf8");
} catch (err) {
    console.log("error");
    console.log(err);
}
var path = require('path')


var cvstorage = multer.diskStorage({
    destination: function(req, file, callback) {
        let dir = `./public/uploads/${POID}/${OID}/`;
        fs.exists(dir, function(e) {
            if (e)
                callback(null, dir);
            else
                fs.mkdir(dir, err => callback(err, dir));
        });
    },
    filename: function(req, file, callback) {

        callback(null, "resume" + "_" + `${req.user.MIN + req.user.MID}` + path.extname(file.originalname).toLowerCase());

    }
});

var cvupload = multer({
    storage: cvstorage
}).any();

var winston = require('../config1/winston');
var browser = require('browser-detect');
var userIP_BRW = "";
var content = "";
parser.parseString(temp, function(err, result) {
    if (!err) {
        MySqlHost = (result.connection.mysqlhost).toString();
        MySqlUser = (result.connection.mysqluser).toString();
        MySqlPassword = (result.connection.mysqlpassword).toString();
        OID = (result.connection.CropID).toString();
        POID = (result.connection.ParentCropID).toString();
        sendermailID = (result.connection.sendermailID).toString();
        Membership_EmailID = (result.connection.Membership_EmailID).toString();
    }
});
var emailstr = fs.readFile(`./public/emailer/${yo.cobj.ParentCropID}/${yo.cobj.CropID}/profileupdate.html`, "utf8", (e, d) => {
    emailstr = d.toString();
});

function getfeed_JSON(mid) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM web_forums.forum_type WHERE OID=${OID}  AND POID=${POID} ;  `;
        con.query(sql, function(err, result) {
            if (err) {
                console.log(err);
                resolve('NA');
            } else {
                let deafault_json = {};
                for (const [i, v] of result.entries()) {
                    deafault_json['FT_ID_' + v.FT_ID] = 1;
                    if (i == result.length - 1) {
                        con.query(`UPDATE web_membership.membermaster SET ModifiedOn=NOW(), ModifiedBY='SYSTEM', M_feed ='${JSON.stringify(deafault_json)}'  where MID=${mid}`, function(err) {
                            if (err) {
                                console.log(err);
                            }
                        });
                        resolve(JSON.stringify(deafault_json));
                    }
                }
            }
        });
    });
}
router.get('/settings', function(req, res) {
    if (req.user) {
        //let pagesql = ` SELECT forum_Type.*,settings.*  FROM web_forums.forum_type AS forum_Type  LEFT JOIN web_forums.forumssettings AS settings  ON forum_Type.FT_ID = settings.FT_ID  WHERE forum_Type.OID = ${OID}`;
        let pagesql = `SELECT * FROM web_forums.forum_type WHERE OID=${OID}  AND POID=${POID} ;  `;
        if (yo.isjson(req.user.M_feed)) {
            con.query(pagesql, (err, result) => {
                if (err)
                    console.log(err);
                else {
                    res.render('settings', {
                        navbar,
                        result
                    });
                }
            });
        } else {
            getfeed_JSON(req.user.MID).then(j => {
                req.user.M_feed = j;
                con.query(pagesql, (err, result) => {
                    if (err)
                        console.log(err);
                    else {
                        res.render('settings', {
                            navbar,
                            result
                        });
                    }
                });
            });
        }
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
});
router.post('/whattosee/:id', function(req, res) {
    if (req.user) {
        let whattosee = JSON.stringify(req.body);
        req.user.M_feed = whattosee;
        let sqlMemberMaster = `UPDATE web_membership.membermaster SET ModifiedOn=NOW(), ModifiedBY='${req.user.M_name}', M_feed ='${whattosee}'  where MID=${req.params.id} `;
        con.query(sqlMemberMaster, function(err, userInfo) {
            if (err) {
                console.log(err);
                req.flash("info", "There was en error try later!");
                res.redirect('/');
            } else {
                req.flash("info", "Successfully updated your preferences");
                res.redirect('/');
            }
        });
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
});
var cityArray = yo.cityArray;
var stateArray = yo.stateArray;
var countryArray = yo.countryArray;
var countrycodeArray = yo.countrycodeArray;
/*function getcityName() {                                                  //fetch data from my sql against each nodalID,shift,oid, date
    setTimeout(function () {
        var sqlquery = "CALL web_membership.GetAllCityNames()";
        con.query(sqlquery, true, (err, rows) => {                       //fetching Vehicle details from mysql
            if (err) {
                console.log(err);
                return;
            }
            for (i in rows[0]) {
                CityArray.push(rows[0][i].CityName);
            }
            for (i in rows[1]) {
                stateArray.push(rows[1][i].StateName);
            }
            for (i in rows[2]) {
                countryArray.push(rows[2][i].CountryName);
            }
        })
    }, 100);
}
getcityName();*/
router.get('/myprofile', function(req, res) {
    if (req.user) {
        // console.log("Req.user: " + JSON.stringify(req.user));
        res.render('myprofile', {
            navbar: navbar
        });
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
});
router.get('/showprofile/:id', function(req, res) {
    if (isNaN(req.params.id)) {
        req.flash("info", "Error! No such profile found!");
        res.redirect('/');
        return;
    }
    con.query('SELECT * FROM web_membership.membermaster where M_Is_Active=1 AND M_Is_deleted=0 AND MID=' + parseInt(req.params.id), (err, result) => {
        if (err)
            console.log(err);
        else {
            if (result.length > 0) {
                yo.decryptmyobjects(result).then((result) => {
                    res.render('showprofile', {
                        result: result,
                        navbar: navbar
                    });
                });
            } else {
                req.flash("info", "The profile you are trying access does not exist");
                res.redirect('/');
            }
        }
    });
});
router.get('/editprofile', function(req, res) {
    if (req.user) {
        userIP_BRW = req.clientIp + " " + browser(req.headers['user-agent']).name;
        if (req.user != null || req.user != undefined) {
            content = [yo.FormattedDate(), req.user.MIN, userIP_BRW, req.user.M_name, "PROFILES", "Browsing", "Pass"].join("   ");
            winston.browse.info(content);
        } else {
            content = [yo.FormattedDate(), "UNAUTHENTICATED", userIP_BRW, "UNAUTHENTICATED", "PROFILES", "Browsing", "Pass"].join("   ");
            winston.browse.info(content);
        }
        winston.browse.info(
            JSON.stringify({
                date_time: new Date(),
                usermin: (req.user) ? req.user.MIN : 'UNAUTHENTICATED',
                userIP: req.clientIp,
                userBRW: browser(req.headers['user-agent']).name,
                username: (req.user) ? req.user.M_name : 'UNAUTHENTICATED',
                activity: req.originalUrl,
                activitytype: 'Browsing',
                activitysuccess: 'Pass'
            }));
        //  console.log(' Req user: ' + JSON.stringify(req.user, null, 2))
        if (req.user.M_Is_CorpMember == 0 || req.user.M_Is_CorpMember == null || req.user.M_Is_CorpMember == undefined) {

            switch (yo.cobj.ParentCropID) {
                case '287':
                    res.render('editprofile_287', {
                        navbar: navbar,
                        countryArray: countryArray,
                        cityArray: cityArray,
                        stateArray: stateArray,
                        countrycodeArray: countrycodeArray,
                        corporatedata: 'NA'
                    });
                    break;
                default:
                    res.render('editprofile', {
                        navbar: navbar,
                        countryArray: countryArray,
                        cityArray: cityArray,
                        stateArray: stateArray,
                        countrycodeArray: countrycodeArray,
                        corporatedata: 'NA'
                    });
                    break;
            }


        } else {
            con.query('SELECT * FROM `web_membership`.`corporate_master` WHERE Corp_Id= ' + req.user.M_Is_CorpMember, (err, result) => {
                if (err) {
                    console.log(err);
                    res.redirect('/');
                } else {
                    yo.decryptmyobjects(result).then((decrytedobjects) => {
                        switch (yo.cobj.ParentCropID) {
                            case '287':
                                res.render('editprofile', {
                                    navbar: navbar,
                                    countryArray: countryArray,
                                    cityArray: cityArray,
                                    stateArray: stateArray,
                                    countrycodeArray: countrycodeArray,
                                    corporatedata: decrytedobjects[0]
                                });
                                break;
                            default:
                                res.render('editprofile', {
                                    navbar: navbar,
                                    countryArray: countryArray,
                                    cityArray: cityArray,
                                    stateArray: stateArray,
                                    countrycodeArray: countrycodeArray,
                                    corporatedata: decrytedobjects[0]
                                });
                                break;
                        }
                    });
                }
            });
        }
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
});
router.post('/edit_cv/:id', function(req, res) {
    if (req.user) {



        cvupload(req, res, function(err) {
            if (err) {
                console.log(err);

                req.flash("info", "Failed to update CV");
                res.redirect('/');
            }
            try {
                let redirecturl = req.body.redirecturl;
                let jid = req.body.jid;
                if (req.files.length != 0) {

                    var resume = req.files[0].filename;
                    req.user.M_CV_File = resume;
                    req.user.M_CV_File_updated_on = new Date();
                    let sqlMemberMaster = "UPDATE web_membership.membermaster SET " + ` ModifiedOn=NOW(), ModifiedBY='${req.user.M_name}', M_CV_File_updated_on=NOW(),  ` + "  M_CV_File = '" + resume + "' where MID= " + req.params.id + "";
                    con.query(sqlMemberMaster, function(err, userInfo) {
                        if (err) {
                            console.log(err)
                        } else {
                            req.flash("info", "Resume updated Successfully");
                            if (redirecturl) {
                                res.redirect(`/openingsView/search?jobid=${jid}&applyingjob=yes`);
                            } else {
                                res.redirect('/profiles/editprofile');
                            }
                        }
                    });
                } else {
                    req.flash("info", "Failed to update CV");
                    res.redirect('/');
                }
            } catch (e) {
                console.log(e);
            }
        });
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
});


// router.post('/edit_vc/:id', function (req, res) {
//     if (req.user) {
//         console.log(" inside edit vc");
//         Upload(req, res, function (err) {
//             if (err) {
//                 console.log(err);
//                 return res.end("Error uploading file.");
//             }
//             try {
//                 if (req.files.length != 0) {
//                     var visitingcard = req.files[0].filename;
//                     req.user.M_VisitingCard = visitingcard;
//                     let sqlMemberMaster = "UPDATE web_membership.membermaster SET  " + ` ModifiedOn=NOW(), ModifiedBY='${req.user.M_name}' ,` + " M_VisitingCard = '" + visitingcard + "' where MID= " + req.params.id + "";
//                     console.log(sqlMemberMaster);
//                     con.query(sqlMemberMaster, function (err, userInfo) {
//                         if (err)
//                             return err;
//                         req.flash("info", "Visiting card updated Successfully");
//                         res.redirect('/profiles/editprofile');
//                     });
//                 } else {
//                     req.flash("info", "Failed to update VC");
//                     res.redirect('/');
//                 }
//             } catch (e) {
//                 console.log(e);
//             }
//         });
//     } else {
//         req.flash("info", "Please login to access this page.");
//         res.redirect('/');
//     }
// });



router.post('/edit_img/:id', (req, res, next) => {
    userIP_BRW = req.clientIp + " " + browser(req.headers['user-agent']).name;
    if (req.user != null || req.user != undefined) {
        content = [yo.FormattedDate(), req.user.MIN, userIP_BRW, req.user.M_name, "PROFILES", "UPDATED PICTURE", "Pass"].join("   ");
        winston.browse.info(content);
    } else {
        content = [yo.FormattedDate(), "UNAUTHENTICATED", userIP_BRW, "UNAUTHENTICATED", "PROFILES", "UPDATED PICTURE", "Pass"].join("   ");
        winston.browse.info(content);
    }
    if (req.user) {
        if (req.user.MID == req.params.id) {
            try {
                if (req.body.imagedata.length > 10) {
                    var base64Data = req.body.imagedata.replace(/^data:image\/jpeg;base64,/, "");
                    var imagefilepath = `./public/uploads/${POID}/${OID}/` + "profileimage" + "_" + `${req.user.MIN + req.user.MID}` + ".jpeg";


                    require("fs").writeFile(imagefilepath, base64Data, 'base64', function(err) {
                        if (err)
                            console.log(err);
                        else {
                            var SelfiLogo = imagefilepath.replace(`./public/uploads/${POID}/${OID}/`, "");
                            req.user.M_Selfi_Logo = SelfiLogo;
                            let sqlMemberMaster = "UPDATE web_membership.membermaster SET  " + ` ModifiedOn=NOW(), ModifiedBY='${req.user.M_name}' ,` + " M_Selfi_Logo = '" + SelfiLogo + "' where MID= " + req.params.id + "";

                            con.query(sqlMemberMaster, function(err, userInfo) {
                                if (err)
                                    return err;
                                req.flash("info", "Profile image updated successfully!");
                                res.redirect('/profiles/editprofile');
                                //res.redirect('/');
                            });
                        }
                    });
                } else {
                    req.flash("info", "Failed to update Profile");
                    // res.redirect('/');
                    res.redirect('/profiles/editprofile');
                }
            } catch (err) {
                console.log(err);
            }
        }
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }
}); // upload complete

router.post('/edit_vistingcard/:id', (req, res, next) => {



    var whatdowecallit = 'visiting card';

    switch (yo.cobj.ParentCropID) {
        case '287':
            whatdowecallit = 'Residence Proof';
            break;
        default:
            whatdowecallit = 'visiting card';
            break;
    }
    userIP_BRW = req.clientIp + " " + browser(req.headers['user-agent']).name;

    if (req.user != null || req.user != undefined) {
        content = [yo.FormattedDate(), req.user.MIN, userIP_BRW, req.user.M_name, `${whatdowecallit}`, "UPDATED PICTURE", "Pass"].join("   ");
        winston.browse.info(content);
    } else {
        content = [yo.FormattedDate(), "UNAUTHENTICATED", userIP_BRW, "UNAUTHENTICATED", `${whatdowecallit}`, "UPDATED PICTURE", "Pass"].join("   ");
        winston.browse.info(content);
    }





    if (req.user) {
        if (req.user.MID == req.params.id) {
            try {
                if (req.body.visitimagedata.length > 10) {
                    var base64Data = req.body.visitimagedata.replace(/^data:image\/jpeg;base64,/, "");
                    var imagefilepath = `./public/uploads/${POID}/${OID}/` + "visitingcard" + "_" + `${req.user.MIN + req.user.MID}` + ".jpeg";
                    require("fs").writeFile(imagefilepath, base64Data, 'base64', function(err) {
                        if (err) {
                            console.log(err);
                            req.flash("info", `Failed to update ${whatdowecallit}`);
                            res.redirect('/profiles/editprofile');
                        } else {
                            var SelfiLogo = imagefilepath.replace(`./public/uploads/${POID}/${OID}/`, "");
                            req.user.M_VisitingCard = SelfiLogo;
                            let sqlMemberMaster = "UPDATE web_membership.membermaster SET  " + ` ModifiedOn=NOW(), ModifiedBY='${req.user.M_name}' ,` + " M_VisitingCard = '" + SelfiLogo + "' where MID= " + req.params.id + "";
                            //console.log(sqlMemberMaster);

                            con.query(sqlMemberMaster, function(err, userInfo) {
                                if (err) {
                                    console.log(err);
                                    req.flash("info", `Failed to update ${whatdowecallit}`);
                                    res.redirect('/profiles/editprofile');
                                } else {
                                    req.flash("info", `${whatdowecallit} updated successfully!`);
                                    res.redirect('/profiles/editprofile');
                                }
                            });
                        }
                    });
                } else {
                    req.flash("info", `Failed to update ${whatdowecallit}`);
                    res.redirect('/profiles/editprofile');
                }
            } catch (err) {
                console.log(err);
                req.flash("info", `Failed to update ${whatdowecallit}`);
                res.redirect('/profiles/editprofile');
            }
        }
    } else {
        req.flash("info", "Please login to access this page.");
        res.redirect('/');
    }









});



// upload complete
// old profile update
// router.post('/edit_img/:id', function (req, res) {
//     if (req.user) {
//         if (req.user.MID == req.params.id) {
//             cpUpload(req, res, (err) => {
//                 if (err) {
//                     console.log(err);
//                     console.log("Something went wrong while uploading image..");
//                 } else {
//                     if (con.state === 'disconnected') {
//                         handleDisconnect(con1);
//                     }
//                     if (req.files.length != 0) {
//                         var SelfiLogo = req.files[0].path;
//                         SelfiLogo = SelfiLogo.replace(/\\/g, "/");
//                         req.user.M_Selfi_Logo = SelfiLogo;
//                         let sqlMemberMaster = "UPDATE web_membership.membermaster SET M_Selfi_Logo = '" + SelfiLogo + "' where MID= " + req.params.id + "";
//                         console.log(sqlMemberMaster);
//                         con.query(sqlMemberMaster, function (err, userInfo) {
//                             if (err)
//                                 return err;
//                             req.flash("info", "Profile Image Updated Successfully");
//                             res.redirect('/');
//                         });
//                     } else {
//                         req.flash("info", "Failed to update Profile");
//                         res.redirect('/');
//                     }
//                 }
//             })
//         }
//     }
// });
router.post('/edit_profile/:id', async function(req, res) {
    if (req.user) {
        if (req.user.MID == req.params.id) {
            //console.log(JSON.stringify(req.body));
            userIP_BRW = req.clientIp + " " + browser(req.headers['user-agent']).name;
            if (req.user != null || req.user != undefined) {
                content = [yo.FormattedDate(), req.user.MIN, userIP_BRW, req.user.M_name, "PROFILES", "EDITED PROFILE", "Pass"].join("   ");
                winston.browse.info(content);
            } else {
                content = [yo.FormattedDate(), "UNAUTHENTICATED", userIP_BRW, "UNAUTHENTICATED", "PROFILES", "EDITED PROFILE", "Pass"].join("   ");
                winston.browse.info(content);
            }
            if (con.state === 'disconnected') {
                handleDisconnect(con1);
            }
            // console.log("-----------------------");
            //console.log(req.body);
            var Address = req.body.M_Personal_Address + ',' + req.body.M_City + ',' + req.body.M_State + ',' + req.body.Personal_Add_Pincode;
            var emailFormat = emailstr.replace("#name#", req.body.M_name);
            emailFormat = emailFormat.replace("#CompanyName#", req.body.M_Company_Name);
            emailFormat = emailFormat.replace("#Address#", Address);
            emailFormat = emailFormat.replace("#EmailId#", req.user.M_EmailID);
            emailFormat = emailFormat.replace("#Phone#", req.body.M_Phone).replace(/#staticurl#/gi, yo.cobj.staticurl)
                .replace(/#POID#/gi, yo.cobj.ParentCropID)
                .replace(/#OID#/gi, yo.cobj.CropID)
                .replace(/ThemeColor/gi, yo.cobj.ThemeColor)
                .replace(/#SiteName#/gi, yo.cobj.SiteName)
                .replace(/#SiteName_full#/gi, yo.cobj.SiteName_full)
                .replace(/#ContactAddress#/gi, yo.cobj.ContactAddress)
                .replace(/#ContactPhone#/gi, yo.cobj.ContactPhone)
                .replace(/#ContactEmail#/gi, yo.cobj.ContactEmail);
            req.user.M_name = req.body.M_name;
            req.user.M_Gender = req.body.M_Gender;
            req.user.M_Phone = req.body.M_Phone;
            req.user.M_Personal_Address = req.body.M_Personal_Address;
            req.user.Personal_Add_Pincode = req.body.Personal_Add_Pincode;
            req.user.M_City = req.body.M_City;
            req.user.M_State = req.body.M_State;
            req.user.M_Country = req.body.M_Country;
            req.user.M_Current_Status = req.body.M_Current_Status;
            req.user.M_CountryCode = req.body.M_CountryCode;
            req.user.M_Overall_Exp = req.body.M_Overall_Exp;
            req.user.M_Company_Name = req.body.M_Company_Name;
            req.user.M_Office_Address = req.body.M_Office_Address;
            req.user.OfficeAdd_Pincode = req.body.OfficeAdd_Pincode;
            req.user.M_Office_City = req.body.M_Office_City;
            req.user.M_Office_State = req.body.M_Office_State;
            req.user.M_Office_Country = req.body.M_Office_Country;
            req.user.M_Current_Role = req.body.M_Current_Role;
            req.user.M_Designation = req.body.M_Designation;
            req.user.M_subscription = req.body.M_subscription;
            var M_name = yo.safe_encrypt(req.body.M_name);
            var M_Gender = yo.safe_encrypt(req.body.M_Gender);
            var M_Phone = yo.safe_encrypt(req.body.M_Phone);
            var M_Personal_Address = yo.safe_encrypt(req.body.M_Personal_Address);
            var Personal_Add_Pincode = yo.safe_encrypt(req.body.Personal_Add_Pincode);
            var M_City = yo.safe_encrypt(req.body.M_City);
            var M_State = yo.safe_encrypt(req.body.M_State);
            var M_Country = yo.safe_encrypt(req.body.M_Country);
            var M_Current_Status = yo.safe_encrypt(req.body.M_Current_Status);
            var TeleCode = yo.safe_encrypt(req.body.M_CountryCode);
            var M_subscription = req.body.M_subscription;
            var M_Company_Name, Designation_, Referal_id, Referal_mobile;
            var M_Office_Address, OfficeAdd_Pincode, M_Official_Email, M_Office_City, M_Office_State;
            var M_Office_Country, M_Current_Role, M_Designation;
            M_Company_Name = yo.safe_encrypt(req.body.M_Company_Name);
            M_Office_Address = yo.safe_encrypt(req.body.M_Office_Address);
            OfficeAdd_Pincode = yo.safe_encrypt(req.body.OfficeAdd_Pincode);
            M_Official_Email = "68bff02bca74aca14b51cfdfa666e36c";
            M_Office_City = yo.safe_encrypt(req.body.M_Office_City);
            M_Office_State = yo.safe_encrypt(req.body.M_Office_State);
            M_Office_Country = yo.safe_encrypt(req.body.M_Office_Country);
            M_Current_Role = yo.safe_encrypt(req.body.M_Current_Role);
            M_Designation = yo.safe_encrypt(req.body.M_Designation);
            if (yo.check(req.body.M_Area_Of_Expert) != 'NA')
                var M_Area_Of_Expert = req.body.M_Area_Of_Expert.toString();
            else
                var M_Area_Of_Expert = 'NA';
            req.user.M_Area_Of_Expert = M_Area_Of_Expert;
            var M_Overall_Exp = yo.safe_encrypt(req.body.M_Overall_Exp);
            var ModifiedBY = req.user.M_name;




            let sqlUpdate = "UPDATE web_membership.membermaster SET M_name = '" + M_name + "',M_Gender = '" + M_Gender + "'," +
                " M_Company_Name = '" + M_Company_Name + "', M_Overall_Exp = '" + M_Overall_Exp + "', M_Phone = '" + M_Phone + "',M_City = '" + M_City + "'," +
                " M_State = '" + M_State + "', M_Country = '" + M_Country + "', M_Official_Email = '" + M_Official_Email + "'," +
                "M_Office_Address = '" + M_Office_Address + "',OfficeAdd_Pincode = '" + OfficeAdd_Pincode + "',M_Office_City = '" + M_Office_City + "'" +
                ",M_Office_State = '" + M_Office_State + "',M_Office_Country = '" + M_Office_Country + "',M_Current_Role = '" + M_Current_Role + "'," +
                " M_Current_Status = '" + M_Current_Status + "', M_Personal_Address = '" + M_Personal_Address + "', Personal_Add_Pincode = '" + Personal_Add_Pincode + "'," +
                " M_subscription = '" + M_subscription + "'," +
                " M_Designation = '" + M_Designation + "', M_Area_Of_Expert = '" + M_Area_Of_Expert + "',M_CountryCode='" + TeleCode + `' ,ModifiedOn=NOW(), ModifiedBY='${ModifiedBY}'   ` + " WHERE MID = '" + req.params.id + "'";
            con.query(sqlUpdate, function(err, result) {
                if (err) {
                    console.log(err)
                    return err;
                } else {
                    emailFormat = emailFormat.replace("#MID#", req.user.MIN);
                    sendMail(emailFormat, req, res, req.user.M_EmailID);
                    req.flash("info", "Profile Updated Successfully");
                    if (req.user.afteroperation) {
                        if ((req.user.afteroperation).includes('InviteeManagement/eventaction?action'))
                            res.redirect(req.user.afteroperation);
                        else
                            res.redirect('/');
                    } else
                        res.redirect('/');
                }
            });
        }
    }
});
router.post('/cedit/:id', (req, res) => {
    if (req.user) {
        console.log(JSON.stringify(req.user));
        if (req.user.M_Is_CorpMember == req.params.id) {
            var Corp_Category = yo.check(req.body.Corp_Category);
            var Corp_BussinessFocus = yo.check(req.body.Corp_BussinessFocus);
            var Corp_Estb_Year = yo.check(req.body.Corp_Estb_Year);
            var Corp_name = yo.safe_encrypt((req.body.Corp_name));
            var Corp_Address = yo.safe_encrypt((req.body.Corp_Address));
            var Corp_Pincode = yo.safe_encrypt((req.body.Corp_Pincode));
            var Corp_City = yo.safe_encrypt((req.body.Corp_City));
            var Corp_State = yo.safe_encrypt((req.body.Corp_State));
            var Corp_Country = yo.safe_encrypt((req.body.Corp_Country));
            var Corp_Landline = yo.safe_encrypt((req.body.Corp_Landline));
            var Corp_weburl = yo.safe_encrypt((req.body.Corp_weburl));
            var Legal_Structure = yo.safe_encrypt((req.body.LegalStructure));
            var Corp_Turnover = yo.safe_encrypt((req.body.Corp_Turnover));
            var Total_Employee = yo.safe_encrypt((req.body.Total_Employee));
            var Corp_Pan = yo.safe_encrypt((req.body.Corp_Pan));
            var Corp_GSTIN = yo.safe_encrypt((req.body.Corp_GSTIN));
            let sqlMemberMaster = `UPDATE web_membership.corporate_master SET Corp_Category = '` + Corp_Category + `',Corp_BussinessFocus = '` + Corp_BussinessFocus + `', Corp_Estb_Year = '` + Corp_Estb_Year + `', Corp_name = '` + Corp_name + `', Corp_Address = '` + Corp_Address + `', Corp_Pincode = '` + Corp_Pincode + `',Corp_City = '` + Corp_City + `', Corp_State = '` + Corp_State + `', Corp_Country = '` + Corp_Country + `',Corp_Landline = '` + Corp_Landline + `',Corp_weburl = '` + Corp_weburl + `',Legal_Structure = '` + Legal_Structure + `',Corp_Turnover = '` + Corp_Turnover + `',Total_Employee = '` + Total_Employee + `', Corp_Pan = '` + Corp_Pan + `', Corp_GSTIN = '` + Corp_GSTIN + `' WHERE Corp_Id = ` + req.params.id;
            let updatecompany = `UPDATE web_membership.membermaster SET M_Company_Name = '` + Corp_name + `' WHERE MID = ` + req.body.cmid;
            // console.log(sqlMemberMaster);
            // console.log(updatecompany);
            con.query(updatecompany, (err, result) => {
                if (err) {
                    console.log(err);
                }
                if (!err) {
                    con.query(sqlMemberMaster, (err, result) => {
                        if (err) {
                            console.log(err);
                            req.flash('info', 'There was an error. Invalid Corporate! details')
                            res.redirect(req.body.pageurl);
                        }
                        if (!err) {
                            req.flash('info', 'Successfully updated the corporate details')
                            res.redirect(req.body.pageurl);
                        }
                    });
                }
            });
        } else {
            req.flash(`info`, `Sorry, You dont have access to view this page.`);
            res.redirect('/');
        }
    } else {
        req.flash(`info`, `Please login to access this page.`);
        res.redirect('/');
    }
});

function sendMail(emailFormat, req, res, Email) {
    var server = email.server.connect({
        //   user: 'cp',
        //    password: 'smart@123',
        host: smtphost,
        port: "25",
        //  ssl:true
    });
    server.send({
        from: sendermailID,
        to: Email,
        bcc: Membership_EmailID,
        subject: `Profile Updated on ${yo.cobj.SiteName} Platform`,
        attachment: [{
            data: emailFormat,
            alternative: true
        }]
    }, function(err, message) {
        if (err) {
            console.log('mail not sent, Error: ' + err);
        } else {
            //  console.log('mail sent');
        }
    });
}
router.get('/rejectionredirector', (req, res) => {
    let {
        action = 'NA'
    } = req.query;
    let validstring = true;
    if (action == 'NA') {
        req.flash('info', 'Invalid URL!');
        res.redirect('/');
    } else {
        try {
            action = cryptr.decrypt(action.toString());
            action = JSON.parse(action);
            //console.log(action);
        } catch (error) {
            validstring = false;
            console.log(error)
        } finally {
            if (validstring) {
                var redirecturl = `${yo.cobj.staticurl}?mid=${action.mid}&email=${action.email}&redirecturl=/profiles/editprofile&templogin=${action.templogin}`;
                console.log(redirecturl);
                if (action.templogin == 'yes') {
                    action.laterdate = new Date(`${action.laterdate}`)
                    let timenow = new Date();
                    let timediffrence = action.laterdate.getTime() - timenow.getTime();
                    let days_remaing = Math.floor(timediffrence / (24 * 60 * 60 * 1000));
                    if (timediffrence >= 0) {
                        res.redirect(redirecturl);
                    } else {
                        req.flash('info', 'Link has expired !')
                        res.redirect('/');
                    }
                } else {
                    res.redirect(redirecturl);
                }
            } else {
                req.flash('info', 'Invalid URL!')
                res.redirect('/');
            }
        }
    }
});
async function haveseennotification(mid) {
    let updateseenresult = await updateseennotification(mid).catch((e) => {
        console.log(e);
    });
    return updateseenresult;
}
router.get('/haveseennotification', (req, res) => {
    if (req.user) {
        haveseennotification(req.user.MID).then((a) => {
            //console.log(a)
            req.user.M_seen_notification = 1;
            res.json({ 'status': 'updated' });
        }).catch((e) => {
            console.log(e);
            res.json({ 'status': 'failed to update seen status' });
        })
    }
});


router.get('/getnotification', async(req, res) => {


    // for mobile app
    if (req.query.mid) {
        let mid = parseInt(req.query.mid);
        let user = await yo.getuserdetail(req.query.mid)
        let { notifications, result } = await getnewcontent_from_lastlogin(user);
        res.json(notifications);

    }


    // for website
    else if (req.user) {
        let { notifications, result } = await getnewcontent_from_lastlogin(req.user);
        res.json(notifications);



    }


});



router.get('/allnotifications', async(req, res) => {
    if (req.user) {
        res.render('notifications', { navbar });
        haveseennotification(req.user.MID);
        req.user.M_seen_notification = 1;
    } else {
        req.flash('info', 'You\'re not logged in!! ');
        res.redirect('/');
    }
});

function updateseennotification(mid) {
    return new Promise((resolve, reject) => {
        let sql = `update web_membership.membermaster set M_seen_notification=1  where MID = ${mid}`;
        con.query(sql, (err, result) => {
            if (err) {
                console.log(err);
                resolve('failed to seen notification');
            } else {
                resolve('updated seen notification');
            }
        });
    });
}

function getnewcontent_from_lastlogin(user) {
    return new Promise((resolve, reject) => {
        let lastlogindate = new Date(user.M_lastlogin)
        let lastloginsqldate = yo.makedate(lastlogindate);
        let { alert_ftid, news_ftid, forum_ftid, knowledge_ftid, article_ftid, whitepaper_ftid, govt_ft_id } = navbar
        let get_newcontent = ` CALL web_events.Newcontent_fromlastlogin ('${yo.cobj.CropID}','${lastloginsqldate}','${alert_ftid}','${news_ftid}' ,  '${forum_ftid}','${knowledge_ftid}','${article_ftid}','${whitepaper_ftid}','${govt_ft_id}' ) `;
        // console.log(get_newcontent)
        con.query(get_newcontent, (err2, result) => {
            if (err2) {
                console.log(err2);
                resolve(false)
            } else {
                let i = {
                    alert: 0,
                    news: 1,
                    forum: 2,
                    knowledge: 3,
                    article: 4,
                    whitepaper: 5,
                    govt: 6,
                    job: 7,
                    upcomingevent: 8,
                    buyer: 9,
                    seller: 10
                };
                //console.log(JSON.stringify(result));
                let notifications = {};
                notifications.lastlogin = lastlogindate;
                notifications.M_seen_notification = user.M_seen_notification;
                notifications.alert = (result[i.alert] && result[i.alert].length > 0) ? result[i.alert] : false;
                notifications.news = (result[i.news] && result[i.news].length > 0) ? result[i.news] : false;
                notifications.forum = (result[i.forum] && result[i.forum].length > 0) ? result[i.forum] : false;
                notifications.knowledge = (result[i.knowledge] && result[i.knowledge].length > 0) ? result[i.knowledge] : false;
                notifications.article = (result[i.article] && result[i.article].length > 0) ? result[i.article] : false;
                notifications.whitepaper = (result[i.whitepaper] && result[i.whitepaper].length > 0) ? result[i.whitepaper] : false;
                notifications.govt = (result[i.govt] && result[i.govt].length > 0) ? result[i.govt] : false;
                notifications.job = (result[i.job] && result[i.job].length > 0) ? result[i.job] : false;
                notifications.upcomingevent = (result[i.upcomingevent] && result[i.upcomingevent].length > 0) ? result[i.upcomingevent] : false;
                notifications.buyer = (result[i.buyer] && result[i.buyer].length > 0) ? result[i.buyer] : false;
                notifications.seller = (result[i.seller] && result[i.seller].length > 0) ? result[i.seller] : false;
                notifications.openurl = {
                    "alert": "NA",
                    "news": "/CMS/newsMain/",
                    "forum": "/forums/show/",
                    "knowledge": "/CMS/knowledgeMain/",
                    "article": "/CMS/articlesMain/",
                    "govt": "/CMS/govtMain/",
                    "whitepaper": "/CMS/whitepaperMain/",
                    "job": "/openingsView/search?jobid=",
                    "upcomingevent": "/events/upcomingevent/",
                    "buyer": "/buyerSection/search?bsid=",
                    "seller": "/sellerSection/search?bsid=",
                }

                //console.log(JSON.stringify(notifications));
                resolve({ notifications, result })
            }
        });
    });
}


router.post('/meminfo', async(req, res) => {

    let value = req.body.value;
    let type = req.body.type;


    let result = { 'status': false, 'user': 'NA' };

    if (value) {
        let user = await FetchMeminfo(type, value);
        if (user && user.length > 0) {
            result.status = true;
            result.user = user[0];
            res.json(JSON.stringify(result));
        } else {
            res.json(JSON.stringify(result));
        }
    } else {
        res.json(JSON.stringify(result));
    }


});


//SELECT SUM(mal_Score) as totalpoints FROM web_membership.member_activity_log WHERE mal_MID= ?
function FetchMeminfo(type, value) {
    return new Promise((resolve, reject) => {

        let getinvitees = "";

        switch (type) {
            case 'mid':
                getinvitees = `SELECT MID,M_name,M_Company_Name,M_Selfi_Logo FROM web_membership.membermaster WHERE MID=${value} `;
                break;
            case 'selfie':
                getinvitees = `SELECT MID,M_Selfi_Logo FROM web_membership.membermaster WHERE  M_Selfi_Logo LIKE '%${value}%' LIMIT 1;`;
                break;
            default:
                getinvitees = `SELECT MID,M_name,M_Company_Name,M_Selfi_Logo FROM web_membership.membermaster WHERE MID=${value} `;
                break;
        }


        con.query(getinvitees, (err2, results) => {
            if (err2) {
                console.log(err2);
                resolve(false)
            } else {
                if (results && results.length > 0) {
                    yo.decryptmyobjects(results).then(members => resolve(members));
                } else resolve(false)
            }
        });
    });
}
/*
Encryted email link in the email.
*/
function getsubscription(mid) {
    return new Promise(function(resolve) {
        let getsubscription_sql = `SELECT M_subscription FROM web_membership.membermaster WHERE membermaster.MID= '${mid}'`
        console.log(getsubscription_sql)
        con.query(getsubscription_sql, (e, r) => {
            if (e) {
                console.log(e)
                resolve([false, 'NA']);
            } else {
                if (r && r[0] && r[0].M_subscription) {
                    resolve([true, r[0].M_subscription]);
                } else {
                    resolve([false, 'NA']);
                }
            }
        });
    });
}

function setsubscription(mid, json) {
    return new Promise(function(resolve) {
        let setsubscription_sql = `UPDATE web_membership.membermaster SET M_subscription = '${json}' WHERE MID = '${mid}' ;`
        con.query(setsubscription_sql, (e, r) => {
            if (e) {
                console.log(e)
                resolve(false);
            } else {
                resolve(true);
            }
        });
    });
}
// let subscription = {
//     newsletter_status: '1',
//     newsletter_unsubscribe: '',
//     alertsms_status: '1',
//     alertsms_unsubscribe: '',
//     jobsmailer_status: '1',
//     jobsmailer_unsubscribe: '',
//     broadcastemail_status: '1',
//     broadcastemail_unsubscribe: '',
// }
// let unsubscribejson = {
//     mid: `${obj.mid}`,
//     type: `${obj.type}`,
//     status: `${onoff}`,
//     reason: `${reason}`
// }
router.post('/subscription_action', async(req, res) => {
    let {
        reason = 'NA',
            key = 'NA',
            onoff = 'NA'
    } = req.body;
    reason = yo.cleantext(reason);
    if ((key == 'NA') || (onoff == 'NA')) {
        req.flash('info', 'Invalid URL!');
        res.redirect('/');
    } else {
        if ((onoff == '0') || (onoff == '1')) {
            let [validstring, obj] = await yo.encString2Obj(key)
            if (validstring) {
                console.log(obj)
                let unsubscribejson = {
                        mid: `${obj.mid}`,
                        type: `${obj.type}`,
                        status: `${onoff}`,
                        reason: `${reason}`
                    }
                    //come here
                let [gotsubscription, subscription] = await getsubscription(obj.mid);
                console.log('before update_subscription_json: ' + subscription);
                if (gotsubscription) {
                    subscription = update_subscription_json(unsubscribejson, subscription)
                    console.log('after update_subscription_json: ' + subscription);
                    let updatestatus = await setsubscription(obj.mid, subscription)
                    if (updatestatus) {
                        req.flash('info', 'Done unsubscribing');
                        res.redirect('/');
                    } else {
                        req.flash('info', 'There was an error while unsubscribing');
                        res.redirect('/');
                    }
                } else {
                    req.flash('info', 'There was an error while unsubscribing');
                    res.redirect('/');
                }
            } else {
                req.flash('info', 'Invalid key!');
                res.redirect('/');
            }
        } else {
            req.flash('info', 'Invalid onoff!');
            res.redirect('/');
        }
    }
});

function update_subscription_json(unsubscribejson, subscription) {
    subscription = subscription.replace(/(\r\n|\n|\r)/g, "");
    subscription = JSON.parse(subscription);
    subscription[unsubscribejson.type + '_status'] = unsubscribejson.status;
    subscription[unsubscribejson.type + '_unsubscribe'] = unsubscribejson.reason;
    subscription = JSON.stringify(subscription);
    console.log(subscription);
    return subscription;
}
router.get('/subscription', (req, res) => {
    let {
        id = 'NA'
    } = req.query;
    let key = req.query.id || 'NA';
    let validstring = true;
    if (id == 'NA') {
        req.flash('info', 'Invalid URL!');
        res.redirect('/');
    } else {
        try {
            id = cryptr.decrypt(id.toString());
            id = id.replace(/(\r\n|\n|\r)/g, "");
            id = JSON.parse(id);
        } catch (error) {
            console.log(error)
            validstring = false;
        } finally {
            setTimeout(() => {
                if (validstring) {
                    res.render('subscription', { navbar, key, id });
                } else {
                    req.flash('info', 'Invalid URL!')
                    res.redirect('/');
                }
            }, 0);
        }
    }
});
module.exports = router;